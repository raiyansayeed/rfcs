RFC: Melee Combat and Progression
=================================

# Problem
A combat system is a core component of making a game fun. We lack a system that requires player skill (aiming and tactic), character progression (upgrade system) and some amount of randomness (don't make each fight the same).


# Current state
Currently one can left click to hurt and knock back an enemy. A rolling mechanic exits and can be used to quickly move towards an enemy or flee from them. On death the player vanishes and can respawn when they wish to.


# Proposer
* Timo Kösters


# Notes
All values given in this RFC are examples and can be changed later on.

*Text written like this will give extra information or the reasoning behind a decision*


# Proposal

## Classes
There are no distinct classes. Your play-style is based on the equipment you have on you and the experience you have with them.

## Melee combat

### Attacking
There are two ways you can directly damage your opponent: Either you make the `Attack` basic move (-> Basic Moves) or you do a short `Advanced Move` (-> Advanced Moves).
*A player most likely won't have time for a longer advanced move, as the enemy will hinder him.*

A normal attack is simple, the player takes the weapon in his hand(s) and uses it. This will deal some amount of damage specified by the weapon he used, unless the enemy avoids it (Blocking or Dodging). After pressing the key a short buildup animation will be played, then the enemy is hit and finally a quick recover animation is played that places the character back into an idle position.

An advanced move results in a special attack. A simple advanced move would be the player getting some distance to his enemy and rushing towards them while holding the `Attack` key. This deals more damage or can have other benefits based on the advanced move.

Advanced moves require more recover time than normal attacks.
*There are no different "skills" that can be activated by specific button bindings. This is confusing because the player has to remember all the keys and it feels limiting to be unable to do a specific move twice in a row when the enemy is not paying attention (cooldowns). Overall, the advanced move system is much more intuitive and realistic and requires more skill and knowledge of the player instead of just pushing a key binding.*

### Blocking
Your shield needs to be held up while you are hit to deny the attack or part of it.

Each shield has a specific pull time which specifies how long it will take to be readied. A shield with a pull time of 0.3 seconds has to be activated at least 0.3 seconds before the attack hits.

A perfect block happens when the player pulls up the shield in the exact moment it is needed. A player needs to be good at predicting an enemy and reading the animations in order to do this. Dependent on the shield used, some bonus could be given to a perfect block.
Examples:
- Enemy is stunned for a short time (<1s)
- Enemy looses energy

### Dodging
A player can roll to get out of range of an enemy. This is fairly energy intensive, so one should only do this when necessary.

At the same time the roll can be used to get behind the enemy and hit him while he is still recovering from his attack.
*This is especially useful when you avoid advanced moves as they have a longer recovery time.*


### Energy based combat
In a fight a player always has to manage his `Energy`. If he tries to hold his shield up forever for example, he'll have to let go at some point, specifically when he doesn't have any energy left.

Each player has an energy value which decreases when he does intensive tasks. When it reaches zero he passes out for a few seconds and is vulnerable to the enemy's attacks.

The energy increases again when a player is out of combat for at least a few seconds.

The whole melee combat system revolves around keeping your energy higher than your opponent's energy. This should be balanced so that it takes 20s on average to exhaust one of the player's energy.

When one player looses all his energy, he is not dead immediately, but he can't defend himself for a few seconds. This gives the opponent a chance to do a special move that quickly drains health (-> Advanced Moves).
*They can be done at any time, but usually the other player will interfere or dodge the attack*

Another option the opponent has is to do a couple of normal attacks and use the rest of the time to max out his energy again. 
*The camera of the exhausted player could move to the enemy to show the epic move he might do.*

When the advanced move succeeds, the opponent will get back up to 90% of his energy and when the exhausted player is ready again, he has 100% energy. That marks the end of one round of combat. When one player uses all of his HP he dies.
*This round based approach is used for multiple reasons: To keep the fight interesting for a longer amount of time by highlighting the special attack one player makes, to give the player with the exhausted character time to breath and prepare for the next round and to make a surprise attack not too powerful.*

### Basic Moves
`Basic Moves` are actions that players can do without much difficulty.

Each Basic Move can be activated by some keyboard/mouse binding and/or based on in-game context (in water you swim).

Basic Moves are not fixed, at any point, a player can gain new basic moves (by learning something or equipping a glider for example) or loose some (by breaking his leg or equipping heavy armor for example).

Activating a basic move can give the player advantages and disadvantages. While sneaking he moves much more quietly and hides near the ground, but he can't defend himself well.

Examples of Basic Moves:
- Run
- Jump
- Roll
- Climb
- Sneak
- Swim
- Glide
- Melee
- Prepared Melee (hold down the attack key)
- Grab (from the ground, pickpocket)
- Block

### Advanced Moves
Building on Basic Moves, there exist `Advanced Moves`, which are combinations of multiple basic moves resulting in one powerful move. Some other games call this a `Combo`.

Many advanced moves need to be unlocked first (-> Unlock System).

When the player triggers different basic moves in a particular order and timing, the advanced move grants special effects like dealing extra damage, changed attack range or tagging the opponent (-> Entity Tags).

They could be coupled with special visual effects like spinning with a weapon held out -> Ring of fire.

Examples of Advanced Moves:
- Run -> Jump -> Melee: Jump onto the enemy (knock-back, reduced armor)
- Run -> Climb -> Jump -> Melee: Ram the weapon into the enemy from above (ignores armor)
- Glide -> Jump -> Melee: Stomp onto the enemy (knock out the enemy, extra damage)
- Idle -> <enemy attacks> Roll -> Melee: Dodge and Attack unprepared enemy (enemy confused, extra damage)
- Sneak -> <enemy unprepared> Melee: Knock out
- Double/Triple Jump -> Melee

## Entity Tags
Every living entity has many different tags that describe it. These can help or hinder what moves you can do or give positive/negative effects (draining health for example).

Examples of Entity Tags:
- Deaf
- On fire / wet
- Confused / ill / dirty

## Item Tags
Every weapon in Veloren is special. Not only do they look different, they also change the way they work. A curved sword might work completely differently to a straight one. Item tags are used to describe the individual characteristics of each item.

Examples of Item Tags:
- Quick/Slow to apply
- Short/Long range
- Thrown (potions can be thrown)
- Piercing/Ignores Armor/Extra Damage
- Magical effects like burning

## Unlock system
Like in real life, you learn something by doing it. A player can collect experience (XP) by killing enemies, finishing quests or doing other actions that might challenge him.

When a player uses his weapon he trains two things: How to handle this weapon type specifically and how to fight in general. Fighting with a short sword both makes you good at using swords and makes you know how to find weak spots of the enemy.

These two different types of experience are stored in two places: The `Class XP Storage`, which is dependent on the weapon you used (weapons resemble classes) and the `Common XP Storage` which is shared by all play-styles.
*This split is made so that a player can't level up his sneaking skills without ever doing anything stealth related. So even advanced players will not be that strong when they fight using a new tactic. Which makes end level play more engaging ("I'm gonna learn how to do ranged combat today, but I'll need to find an easy target"). Even high level players need to be cautious.*

To upgrade a character a tree based structure is used. Each upgrade requires a certain amount of XP to get and you have to own all previous upgrades already. Each weapon requires a certain amount of class XP and a certain amount of common XP. Some niche weapons could be 90% class XP and only 10% common XP while it could be flipped for some other weapons.
